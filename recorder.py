#!/usr/bin/env python3

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Gst', '1.0')
from gi.repository import Gtk, Gst, GLib, Gio

class Recorder:
    pipeline = None
    audio_devices = []
    video_devices = []
    ostream = None
    title = "VHS Recorder"
    filename = None

    def __init__(self, builder):
        self.window = builder.get_object("MainWindow")
        self.audio_monitor = Gst.DeviceMonitor()
        self.video_monitor = Gst.DeviceMonitor()
        self.audio_combo = builder.get_object("AudioSourceCombo")
        self.video_combo = builder.get_object("VideoSourceCombo")
        self.error_bar = builder.get_object("InfoBar")
        self.error_label = builder.get_object("ErrorLabel")
        self.record_button = builder.get_object("RecordButton")
        self.capture_width_spin = builder.get_object("CaptureWidthSpinButton")
        self.capture_height_spin = builder.get_object("CaptureHeightSpinButton")
        self.add_borders_check = builder.get_object("AddBordersCheck")
        self.output_width_spin = builder.get_object("OutputWidthSpinButton")
        self.output_height_spin = builder.get_object("OutputHeightSpinButton")
        self.framerate_num_spin = builder.get_object("FramerateNumSpinButton")
        self.framerate_denom_spin = builder.get_object("FramerateDenomSpinButton")
        self.configuration_panel = builder.get_object("ConfigurationPanel")
        self.header_bar = builder.get_object("HeaderBar")

        self.audio_monitor.add_filter("Source/Audio")
        bus = self.audio_monitor.get_bus()
        bus.add_signal_watch()
        bus.connect("message::device-added", self.onAudioDeviceAdded)
        bus.connect("message::device-removed", self.onAudioDeviceRemoved)

        self.video_monitor.add_filter("Source/Video")
        bus = self.video_monitor.get_bus()
        bus.add_signal_watch()
        bus.connect("message::device-added", self.onVideoDeviceAdded)
        bus.connect("message::device-removed", self.onVideoDeviceRemoved)

        self.audio_monitor.start()
        self.video_monitor.start()

        self.audio_devices.append(None)
        for device in self.audio_monitor.get_devices():
            self.addAudioDevice(device)

        self.video_devices.append(None)
        for device in self.video_monitor.get_devices():
            self.addVideoDevice(device)

        self.video_sink = Gst.ElementFactory.make("gtkglsink", None)
        video_overlay = builder.get_object("VideoOverlay")
        video_overlay.add_overlay(self.video_sink.props.widget)

        self.directory = GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_VIDEOS)

    def showError(self, msg):
        self.error_label.set_text(msg)
        self.error_bar.set_visible(True)

    def clearError(self):
        self.error_bar.set_visible(False)

    def createPipeline(self, pipeline_str):
        if self.pipeline:
            self.pipeline.set_state(Gst.State.NULL)
            inner_bin = self.pipeline.get_by_name("inner_bin")
            self.pipeline.remove(inner_bin)
            inner_bin = None
            self.pipeline = None

        audio_device = self.audio_devices[self.audio_combo.get_active()]
        video_device = self.video_devices[self.video_combo.get_active()]

        pipeline_params = {
            "capture_width": int(self.capture_width_spin.get_value()),
            "capture_height": int(self.capture_height_spin.get_value()),
            "add_borders": int(self.add_borders_check.get_active()),
            "output_width": int(self.output_width_spin.get_value()),
            "output_height": int(self.output_height_spin.get_value()),
            "framerate_num": int(self.framerate_num_spin.get_value()),
            "framerate_denom": int(self.framerate_denom_spin.get_value()),
        }
        pipeline_str = pipeline_str.format(**pipeline_params)
        print("Starting pipeline:" + pipeline_str)

        self.pipeline = Gst.Pipeline()
        audio_source = audio_device.create_element()
        video_source = video_device.create_element()
        inner_bin = Gst.parse_bin_from_description (pipeline_str, False)
        inner_bin.set_name("inner_bin")
        audio_queue = inner_bin.get_by_name("audio_input")
        video_input = inner_bin.get_by_name("video_input")
        video_sink = inner_bin.get_by_name("video_sink")

        self.pipeline.add(audio_source)
        self.pipeline.add(video_source)
        self.pipeline.add(inner_bin)
        video_sink.props.sink = self.video_sink

        if not audio_source.link(audio_queue):
            self.showError("Could not link audio source.")
            self.hidePreview()
            return

        if not video_source.link(video_input):
            self.showError("Could not link video source.")
            self.hidePreview()
            return

        bus = self.pipeline.get_bus()
        bus.add_signal_watch()
        bus.connect("message::eos", self.onEos)

        return inner_bin

    def startRecording(self):
        audio_device = self.audio_devices[self.audio_combo.get_active()]
        if not audio_device:
            self.showError("You need to select an audio capture device.")
            self.record_button.set_active(False)
            return
        audio_source = audio_device.create_element()

        video_device = self.video_devices[self.video_combo.get_active()]
        if not video_device:
            self.showError("You need to select a video capture device.")
            self.record_button.set_active(False)
            return
        video_source = video_device.create_element()

        if not self.ostream:
            self.showError("You need to select a file to save to.")
            self.record_button.set_active(False)
            return

        self.configuration_panel.set_sensitive(False)

        record_str = """
            tee name=audio_input
            capsfilter name=video_input caps="video/x-raw,width={capture_width},height={capture_height},framerate={framerate_num}/{framerate_denom}" !
              videoscale add-borders={add_borders} !
              video/x-raw,width={output_width},height={output_height},pixel-aspect-ratio=1/1 !
              videoconvert ! tee name=video_tee
            video_tee. ! queue ! timeoverlay time-mode=running-time halignment=center valignment=bottom !
              glsinkbin name=video_sink
            video_tee. ! queue ! vaapih264enc tune=high-compression keyframe-period=120 max-bframes=0 quality-level=1 ! mux.
            audio_input. ! queue ! pulsesink buffer-time=80000
            audio_input. ! queue ! avenc_aac ! mux.
            mp4mux name=mux ! giostreamsink name=file_sink
        """

        inner_bin = self.createPipeline(record_str)
        if inner_bin:
            file_sink = inner_bin.get_by_name("file_sink")
            file_sink.props.stream = self.ostream
            self.pipeline.set_state(Gst.State.PLAYING)
            self.video_sink.props.widget.show()

    def stopRecording(self):
        if self.pipeline:
            self.record_button.set_sensitive(False)
            self.pipeline.send_event(Gst.Event.new_eos())

    def showPreview(self):
        preview_str = """
            capsfilter name=video_input caps="video/x-raw,width={capture_width},height={capture_height},framerate={framerate_num}/{framerate_denom}" !
              videoscale add-borders={add_borders} !
              video/x-raw,width={output_width},height={output_height},pixel-aspect-ratio=1/1 !
              queue name=video_queue ! glsinkbin name=video_sink
            queue name=audio_input ! pulsesink
        """

        inner_bin = self.createPipeline(preview_str)
        if inner_bin:
            self.pipeline.set_state(Gst.State.PLAYING)
            self.video_sink.props.widget.show()

    def hidePreview(self):
        if self.pipeline:
            self.pipeline.set_state(Gst.State.NULL)
            self.pipeline = None
        self.video_sink.props.widget.hide()

    def onDestroy(self, *args):
        Gtk.main_quit()

    def onEos(self, bus, message):
        print(message.src)
        if self.ostream:
            self.ostream.flush()
            self.ostream.close()
            self.ostream = None

        self.record_button.set_sensitive(True)
        self.configuration_panel.set_sensitive(True)
        self.header_bar.set_title(self.title)
        self.showPreview()

    def onSaveAs(self, *args):
        self.clearError()
        dialog = Gtk.FileChooserDialog(title = "Record To",
                                       parent = self.window,
                                       action = Gtk.FileChooserAction.SAVE)
        dialog.add_button ("Cancel", Gtk.ResponseType.CANCEL)
        dialog.add_button ("Create", Gtk.ResponseType.ACCEPT)
        dialog.set_do_overwrite_confirmation(True)
        dialog.set_current_folder(self.directory)
        res = dialog.run()
        if res == Gtk.ResponseType.ACCEPT:
            self.directory = dialog.get_current_folder()
            file = dialog.get_file()
            title = self.title + " - " + file.get_basename()
            self.header_bar.set_title(title)
            self.ostream = file.replace(None, False, Gio.FileCreateFlags.REPLACE_DESTINATION, None)
        dialog.destroy()

    def onCloseError(self, *args):
        self.error_bar.set_visible(False)

    def onSourceChanged(self, *args):
        self.clearError()
        audio_device = self.audio_devices[self.audio_combo.get_active()]
        video_device = self.video_devices[self.video_combo.get_active()]
        if audio_device and video_device:
            self.showPreview()
        else:
            self.hidePreview()

    def onRecordToggled(self, *args):
        if self.record_button.get_active():
            self.startRecording()
        else:
            self.stopRecording()

    def onAudioDeviceAdded(self, bus, message):
        device = message.parse_device_added()
        self.addAudioDevice(device)

    def onAudioDeviceRemoved(self, bus, message):
        device = message.parse_device_removed()
        position = self.removeDevice(self.audio_devices, device)
        if position:
            self.audio_combo.remove(position)
            print("Removed audio capture: %s" % device.get_display_name())
        else:
            print("WARNING: Could not remove audio device '%s' (%s)" %
                    (device.get_display_name(), str(device)))

    def onVideoDeviceAdded(self, bus, message):
        device = message.parse_device_added()
        self.addVideoDevice(device)

    def onVideoDeviceRemoved(self, bus, message):
        device = message.parse_device_removed()
        position = self.removeDevice(self.video_devices, device)
        self.video_combo.remove(position)
        print("Removed audio capture: %d: %s" % (position, device.get_display_name()))

    def addAudioDevice(self, device):
        position = self.addDevice(self.audio_devices, device)
        self.audio_combo.insert_text(position, device.get_display_name())
        print("Got audio capture: %s" % device.get_display_name())

    def addVideoDevice(self, device):
        position = self.addDevice(self.video_devices, device)
        self.video_combo.insert_text(position, device.get_display_name())
        print("Got video capture: %s" % device.get_display_name())

    def addDevice(self, devices, device):
        for i in range(1, len(devices)):
            if device == devices[i]:
                return i
            if device.get_display_name() > devices[i].get_display_name():
                devices.insert(i, device)
                return i
        devices.insert(1, device)
        return 1

    def removeDevice(self, devices, device):
        for i in range(1, len(devices)):
            if devices[i] == device:
                del devices[i]
                return i


if __name__ == "__main__":
    Gst.init(None)
    builder = Gtk.Builder()
    builder.add_from_file("recorder-ui.glade")
    builder.connect_signals(Recorder(builder))
    window = builder.get_object("MainWindow")
    window.show()
    Gtk.main()
